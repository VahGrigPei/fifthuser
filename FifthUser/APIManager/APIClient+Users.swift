//
//  APIClient+Users.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import Alamofire

extension APIClient {
    
    @discardableResult func requestUsersFor(page: Int, completion: @escaping (UserData?, Error?) -> Void) -> DataRequest {
        
        let url = apiUrl(for: "api/users?page=\(page)")
        return manager.request(url,
                               method: .get,
                               parameters: nil,
                               encoding: JSONEncoding.default)
            .validateForFifthUserApiError()
            .responseJSONDecodable { (response: DataResponse<UserData>) in
                completion(response.value, response.error)
        }
    }
    
    @discardableResult func requestUserWith(id: Int, completion: @escaping (User?, Error?) -> Void) -> DataRequest {
        
        let url = apiUrl(for: "api/users/\(id)")
        return manager.request(url,
                               method: .get,
                               parameters: nil,
                               encoding: JSONEncoding.default)
            .validateForFifthUserApiError()
            .responseJSONDecodable { (response: DataResponse<User>) in
                completion(response.value, response.error)
        }
    }
    
}
