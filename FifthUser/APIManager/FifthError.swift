//
//  FifthError.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

struct TechTaskError: LocalizedError, Codable {
    
    let error: String?
    
    var errorDescription: String? { return error }
    var localizedDescription: String? { return error }
    
}
