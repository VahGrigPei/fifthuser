//
//  APIError.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

enum APIError: Int, LocalizedError {
    
    case unknownError
    
    public var errorDescription: String? {
        switch self {
        case .unknownError: return "Unknown server error"
        }
    }
    
}
