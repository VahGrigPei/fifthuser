//
//  APIClient.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import Alamofire

class APIClient: RequestAdapter {
    
    private let apiUrl: URL
    static let mainUrl = "https://reqres.in/"
    let manager: SessionManager

    static let shared: APIClient = APIClient(apiUrl: URL(string: mainUrl)!, configuration: .default)
    
    init(apiUrl: URL, configuration: URLSessionConfiguration) {
        self.apiUrl = apiUrl
        self.manager = SessionManager(configuration: configuration)
        self.manager.adapter = self
    }
    
    func apiUrl(for path: String) -> URL {
        return URL(string: APIClient.mainUrl + path)!
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var request = urlRequest
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        return request
    }
    
}
