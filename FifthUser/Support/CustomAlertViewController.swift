//
//  CustomAlertViewController.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/10/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit
import Foundation

struct CustomAlertViewController {
    let alertController: UIAlertController

    init(title: String?, message: String?) {
        self.alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    }

    func addAction(title: String, _ handler: ((UIAlertAction) -> Void)? = nil) -> CustomAlertViewController {
        return self.addAction(title: title, style: .default, handler)
    }

    func addAction(title: String, style: UIAlertAction.Style,
                   _ handler: ((UIAlertAction) -> Void)? = nil) -> CustomAlertViewController {
        alertController.addAction(UIAlertAction(title: title, style: style, handler: handler))
        return self
    }

    func showOn(viewController: UIViewController) {
        viewController.present(alertController, animated: true, completion: nil)
    }
    
}
