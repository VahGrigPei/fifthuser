//
//  User.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation

class UserData: Codable {
    
    let page: Int?
    let per_page: Int?
    let total: Int?
    let total_pages: Int?
    let data: [User]?
}
