//
//  User.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object, Codable {
    @objc dynamic var id = 0
    @objc dynamic var email: String? = ""
    @objc dynamic var first_name: String? = ""
    @objc dynamic var last_name: String? = ""
    @objc dynamic var avatar: String? = ""
    @objc dynamic var avatarImage: Data? = Data()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
