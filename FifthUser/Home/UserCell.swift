//
//  UserCell.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit
import SDWebImage

class UserCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userEmailLabel: UILabel!
    
    var user: User? {
        didSet {
            if let avatarString = user?.avatar, let avatarUrl = URL(string: avatarString) {
                userImageView.sd_setImage(with: avatarUrl) { (image, error, cashtype, url) in
                }
            }
            userEmailLabel.text = user?.email
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
