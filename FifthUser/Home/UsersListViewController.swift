//
//  UsersListViewController.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit

class UsersListViewController: UITableViewController {
    
    var page = 1
    let cellName = "UserCell"
    let segueName = "UserInfoSegue"
    
    lazy var viewModel: UsersListViewModel = {
        return UsersListViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFirstPage()
    }
    
    func loadFirstPage() {
        viewModel.loadUsers(page: page - 1)
        viewModel.onUserSet = { [weak self] in
            self?.tableView.reloadData()
        }
        
        viewModel.onError = { error in
            CustomAlertViewController(title: error?.localizedDescription,
                                      message: nil)
                .addAction(title: "Ok").showOn(viewController: self)
        }
        
        self.tableView.tableFooterView = UIView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let descriptionVC = segue.destination as? UserViewController
        descriptionVC?.viewModel.user = sender as? User
    }
    
}

extension UsersListViewController: UITableViewDataSourcePrefetching {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchedUsers?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellName, for: indexPath) as! UserCell
        if let user = viewModel.searchedUsers?[indexPath.row] {
            cell.user = user
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if let cell = tableView.cellForRow(at: indexPath) as? UserCell {
            performSegue(withIdentifier: segueName, sender: cell.user)
        }
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        let upcomingRows = indexPaths.map { $0.row }
        if let maxIndex = upcomingRows.max(),
            let pageSize = viewModel.usersData?.per_page,
            let totalPages = viewModel.usersData?.total_pages {
            let nextPage: Int = Int(ceil(Double(maxIndex) / Double(pageSize))) + 1
            
            if nextPage > page && (page < (totalPages)) {
                viewModel.loadUsers(page: nextPage)
                page = nextPage
            }
        }
    }
}

extension UsersListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.usersFor(searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        viewModel.usersFor(searchText: "")
        self.view.endEditing(true)
    }
    
}

