//
//  UserViewModel.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import RealmSwift

class UserViewModel {
    
    var realm: Realm?
    var user: User?
    var onUserSet: (() -> Void)?
    
    func loadUser() {
        guard let userId = self.user?.id else { return }
        APIClient.shared.requestUserWith(id: userId) { (user, error) in
            if let user = user {
                self.user = user
                self.saveUsers(users: [user])
                self.onUserSet?()
            }
        }
    }
    
    func saveUsers(users: [User]) {
        do {
            realm = try Realm()
            try? realm?.write {
                realm?.add(users, update: .modified)
                onUserSet?()
            }
        } catch {
            print("no realm!!!")
        }
    }
    
}
