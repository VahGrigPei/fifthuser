//
//  File.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import Foundation
import RealmSwift

class UsersListViewModel {
    
    var realm: Realm?
    var usersData: UserData?
    var isFetching = false
    var onUserSet: (() -> Void)?
    var onError: ((Error?) -> Void)?
    
    var users: [User]? {
        return savedUsers()
    }
    var searchedUsers: [User]? {
        didSet {
            self.onUserSet?()
        }
    }
    
    func loadUsers(page: Int) {
        isFetching = true
        APIClient.shared.requestUsersFor(page: page) { (usersData, error) in
            self.isFetching = false
            if error != nil {
                self.onError?(error)
            } else if let users = usersData?.data {
                self.usersData = usersData
                self.saveUsers(users: users)
                self.searchedUsers = self.users
            }
        }
    }
    
    func saveUsers(users: [User]) {
        do {
            realm = try Realm()
            try? realm?.write {
                realm?.add(users, update: .modified)
                onUserSet?()
            }
        } catch {
            print("no realm!!!")
        }
    }
    
    func savedUsers() -> [User]? {
        do {
            realm = try Realm()
            if let users = realm?.objects(User.self).toArray(ofType: User.self) {
                return users
            }
        } catch {
            print("no realm!!!")
        }
        return nil
    }
    
    func usersFor(searchText: String) {
        if searchText.isEmpty {
            searchedUsers = users
        } else {
            guard let users = self.users else { return }
            let prefixArray = users.filter {($0.email?.hasPrefix(searchText) ?? false)}
            let containArray = users.filter {((($0.email?.containsIgnoringCase(find: searchText)) ?? false) && !prefixArray.contains(where: { $0.email == $0.email}))}
            searchedUsers = prefixArray + containArray
        }
    }
    
}


