//
//  UserViewController.swift
//  FifthUser
//
//  Created by Vahan Grigoryan on 9/7/19.
//  Copyright © 2019 Vahan Grigoryan. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!

    lazy var viewModel: UserViewModel = {
        return UserViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }
    
    func initialConfig() {
        self.configUI()
        viewModel.loadUser()
        viewModel.onUserSet = {
            self.configUI()
        }
    }
    
    func configUI() {
        if let user = viewModel.user {
            emailLabel.text = user.email
            nameLabel.text = (user.first_name ?? "") + " " + (user.last_name ?? "")
            if let avatarString = user.avatar, let avatarUrl = URL(string: avatarString) {
                userImageView.sd_setImage(with: avatarUrl) { (image, error, cashtype, url) in
                }
            }
        }
    }
    
}
